package ru.itis.exception.post;

import ru.itis.exception.RusappForbiddenException;

public class PostForbiddenException extends RusappForbiddenException {
    public PostForbiddenException() {
        super("This post forbidden for you");
    }
}
