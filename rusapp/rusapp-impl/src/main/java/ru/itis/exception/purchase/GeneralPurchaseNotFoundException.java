package ru.itis.exception.purchase;

import ru.itis.exception.RusappNotFoundException;

public class GeneralPurchaseNotFoundException extends RusappNotFoundException {
    public GeneralPurchaseNotFoundException() {
        super("General purchases not found");
    }
}
