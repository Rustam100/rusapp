package ru.itis.exception.user;

import org.springframework.http.HttpStatus;
import ru.itis.exception.RusappServiceException;

public class TokenRefreshException extends RusappServiceException {

    public TokenRefreshException(String token, String message) {
        super(HttpStatus.UNAUTHORIZED, String.format("Failed for [%s]: %s", token, message));
    }
}
