package ru.itis.exception.purchase;

import ru.itis.exception.RusappForbiddenException;

public class PurchaseForbiddenException extends RusappForbiddenException {

    public PurchaseForbiddenException() {
        super("This action forbidden for this purchase");
    }
}
