package ru.itis.exception.category;

import ru.itis.exception.RusappNotFoundException;

public class CategoryNotFoundException extends RusappNotFoundException {
    public CategoryNotFoundException() {
        super("Category not found");
    }
}
