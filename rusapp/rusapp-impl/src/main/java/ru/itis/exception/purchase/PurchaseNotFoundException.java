package ru.itis.exception.purchase;

import ru.itis.exception.RusappNotFoundException;

public class PurchaseNotFoundException extends RusappNotFoundException {

    public PurchaseNotFoundException() {
        super("Purchase not found");
    }
}
