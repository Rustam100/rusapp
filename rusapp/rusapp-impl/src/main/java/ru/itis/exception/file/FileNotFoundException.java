package ru.itis.exception.file;

import ru.itis.exception.RusappNotFoundException;

public class FileNotFoundException extends RusappNotFoundException {
    public FileNotFoundException() {
        super("File not found");
    }
}
