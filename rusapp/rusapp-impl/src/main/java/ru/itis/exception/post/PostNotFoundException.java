package ru.itis.exception.post;

import ru.itis.exception.RusappNotFoundException;

public class PostNotFoundException extends RusappNotFoundException {
    public PostNotFoundException() {
        super("Post not found");
    }
}
