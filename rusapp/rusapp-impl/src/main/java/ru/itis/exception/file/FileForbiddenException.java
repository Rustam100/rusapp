package ru.itis.exception.file;

import ru.itis.exception.RusappForbiddenException;

public class FileForbiddenException extends RusappForbiddenException {
    public FileForbiddenException() {
        super("This file forbidden for you");
    }
}
