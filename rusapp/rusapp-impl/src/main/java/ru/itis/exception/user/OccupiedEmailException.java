package ru.itis.exception.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class OccupiedEmailException extends RuntimeException {
    public OccupiedEmailException(String msg) {
        super(msg);
    }
}
