package ru.itis.exception;

import org.springframework.http.HttpStatus;

public class RusappNotFoundException extends RusappServiceException {

    public RusappNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}

