package ru.itis.exception.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FailureLoginException extends RuntimeException {
}
