package ru.itis.exception.user;

import ru.itis.exception.RusappNotFoundException;

public class RoleNotFoundException extends RusappNotFoundException {

    public RoleNotFoundException() {
        super("Role not found");
    }
}
