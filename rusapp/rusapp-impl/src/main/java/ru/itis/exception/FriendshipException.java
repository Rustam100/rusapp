package ru.itis.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FriendshipException extends RuntimeException {


    public FriendshipException(String msg) {
        super(msg);
    }
}
