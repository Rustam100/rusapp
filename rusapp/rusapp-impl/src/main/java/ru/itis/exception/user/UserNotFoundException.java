package ru.itis.exception.user;

import ru.itis.exception.RusappNotFoundException;

public class UserNotFoundException extends RusappNotFoundException {

    public UserNotFoundException() {
        super("User not found");
    }
}

