package ru.itis.service;

import ru.itis.dto.response.PurchaseStatisticResponse;


public interface StatisticsService {

    PurchaseStatisticResponse geGeneralPurchaseStatistics(String username, String from, String to);
    PurchaseStatisticResponse getPurchaseStatistics(String username, String from, String to);
}
