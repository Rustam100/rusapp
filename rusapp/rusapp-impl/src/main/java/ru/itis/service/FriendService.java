package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.response.UserResponse;

import java.util.List;

public interface FriendService {

    void sendFriendRequest(UserDetails userDetails,
                           String invitedUserId);

    void acceptFriendRequest(UserDetails userDetails,
                             String invitingUserId,
                             String action);

    List<UserResponse> getFriends(UserDetails userDetails,
                                  String section);

    List<UserResponse> getRequests(UserDetails userDetails);

    List<UserResponse> getAllFriends(UserDetails userDetails);

    void deleteFriend(UserDetails userDetails,
                      String username);
}
