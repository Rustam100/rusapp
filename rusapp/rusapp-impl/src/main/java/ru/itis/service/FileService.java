package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.FileResponse;

import java.util.List;
import java.util.UUID;

public interface FileService {

    FileResponse getFile(UUID fileId);

    List<FileResponse> uploadToPost(UserDetails userDetails, UUID postId, MultipartFile[] multipartFiles);

    List<FileResponse> uploadToPurchase(UserDetails userDetails, UUID purchaseId, MultipartFile[] multipartFiles);

    void deletePostFile(UserDetails userDetails, UUID postId, UUID fileId);

    void deletePurchaseFile(UserDetails userDetails, UUID purchaseId, UUID fileId);
}
