package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreatePurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.PurchaseResponse;

import java.util.List;
import java.util.UUID;

public interface PurchaseService {

    PurchaseResponse getPurchase(UUID purchaseId);

    List<PurchaseResponse> getUserPurchases(String username, int page, int size);

    List<PurchaseResponse> searchPurchasesByCriteria(int page, int size, String criteria);

    PurchaseResponse addNewPurchase(UserDetails userDetails, CreatePurchaseRequest createPurchaseRequest);

    PurchaseResponse updatePurchase(UserDetails userDetails, UUID purchaseId, UpdatePurchaseRequest request);

    void deletePurchase(UserDetails userDetails, UUID purchaseId);
}
