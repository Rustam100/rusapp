package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreatePostRequest;
import ru.itis.dto.request.UpdatePostRequest;
import ru.itis.dto.response.PostResponse;
import ru.itis.enums.PostState;
import ru.itis.exception.post.PostForbiddenException;
import ru.itis.exception.post.PostNotFoundException;
import ru.itis.exception.purchase.PurchaseNotFoundException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.PostEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.PostRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.PostService;
import ru.itis.utils.mapper.PostMapper;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final PostMapper postMapper;

    @Override
    public PostResponse createPost(CreatePostRequest postRequest, UserDetails userDetails) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);

        PostEntity post = postMapper.toPostEntityFromCreateRequest(postRequest);
        post.setAuthor(user);
        post.setState(PostState.NOT_DELETED);

        return postMapper.toPostResponse(postRepository.save(post));
    }

    @Override
    public List<PostResponse> getPosts(UserDetails userDetails, int page, int size) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        PageRequest request = PageRequest.of(page, size);
        Page<PostEntity> result = postRepository.findAllByAuthorAndState(request, user, PostState.NOT_DELETED)
                .orElseThrow(PurchaseNotFoundException::new);
        return postMapper.toPostResponse(result.getContent());
    }

    @Override
    public PostResponse getPostById(String postId) {
        PostEntity post = postRepository.findById(UUID.fromString(postId))
                .orElseThrow(PostNotFoundException::new);
        return postMapper.toPostResponse(post);
    }

    @Override
    public PostResponse updatePostById(UserDetails userDetails, UpdatePostRequest postRequest, String postId) {
        PostEntity post = postRepository.findById(UUID.fromString(postId))
                .orElseThrow(PostNotFoundException::new);
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        if (checkPostByOwn(user.getUuid(), UUID.fromString(postId))) {
            postMapper.updatePost(post, postRequest);
            return postMapper.toPostResponse(postRepository.save(post));
        } else {
            throw new PostForbiddenException();
        }
    }

    @Override
    public void deletePostById(UserDetails userDetails, String postId) {
        PostEntity post = postRepository.findById(UUID.fromString(postId))
                .orElseThrow(PostNotFoundException::new);
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        if (checkPostByOwn(user.getUuid(), post.getUuid())) {
            post.setState(PostState.DELETED);
            postRepository.save(post);
        } else {
            throw new PostForbiddenException();
        }
    }

    @Override
    public List<PostResponse> getUserPostsByUsername(String username, int page, int size) {
        UserEntity user = userRepository.findOneByUsername(username)
                .orElseThrow(UserNotFoundException::new);
        PageRequest request = PageRequest.of(page, size);
        Page<PostEntity> result = postRepository.findAllByAuthorAndState(request, user, PostState.NOT_DELETED)
                .orElseThrow(PostNotFoundException::new);
        return postMapper.toPostResponse(result.getContent());
    }

    @Override
    public boolean checkPostByOwn(UUID authorId, UUID postId) {
        Integer count = postRepository.checkPostByAuthorOwn(authorId, postId);
        return count > 0;
    }
}
