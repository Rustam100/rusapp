package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.FileResponse;
import ru.itis.enums.FileState;
import ru.itis.exception.file.FileNotFoundException;
import ru.itis.exception.post.PostForbiddenException;
import ru.itis.exception.post.PostNotFoundException;
import ru.itis.exception.purchase.PurchaseForbiddenException;
import ru.itis.exception.purchase.PurchaseNotFoundException;
import ru.itis.model.FileEntity;
import ru.itis.model.PostEntity;
import ru.itis.model.PurchaseEntity;
import ru.itis.repository.FileRepository;
import ru.itis.repository.PostRepository;
import ru.itis.repository.PurchaseRepository;
import ru.itis.service.FileService;
import ru.itis.utils.mapper.FileMapper;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;
    private final FileMapper fileMapper;

    private final PurchaseRepository purchaseRepository;
    private final PostRepository postRepository;

    @Value("${files.storage.path}")
    private String storagePath;

    @Override
    public FileResponse getFile(UUID fileId) {
        return fileMapper.toFileResponse(fileRepository.findByUuidAndState(fileId, FileState.OK)
                .orElseThrow(FileNotFoundException::new));
    }

    @Override
    public List<FileResponse> uploadToPost(UserDetails userDetails, UUID postId, MultipartFile[] multipartFiles) {

        PostEntity post = postRepository.findById(postId).orElseThrow(PostNotFoundException::new);
        ownCheckPost(userDetails, post);
        List<FileEntity> files = new ArrayList<>();
        for (MultipartFile file: multipartFiles) {
            try {
                Files.copy(file.getInputStream(), Paths.get(storagePath, file.getOriginalFilename()));
                FileEntity fileEntity= getFile(file);
                fileEntity.setPost(post);
                files.add(fileRepository.save(fileEntity));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileMapper.toFileResponseList(files);
    }

    @Override
    public List<FileResponse> uploadToPurchase(UserDetails userDetails, UUID purchaseId, MultipartFile[] multipartFiles) {

        PurchaseEntity purchase = purchaseRepository.findById(purchaseId).orElseThrow(PurchaseNotFoundException::new);
        ownCheckPurchase(userDetails, purchase);
        List<FileEntity> files = new ArrayList<>();
        for (MultipartFile file: multipartFiles) {
            try {
                Files.copy(file.getInputStream(), Paths.get(storagePath, file.getOriginalFilename()));
                FileEntity fileEntity= getFile(file);
                fileEntity.setPurchase(purchase);
                files.add(fileRepository.save(fileEntity));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileMapper.toFileResponseList(files);
    }

    @Transactional
    @Override
    public void deletePostFile(UserDetails userDetails, UUID postId, UUID fileId) {
        PostEntity post = postRepository.findById(postId).orElseThrow();
        ownCheckPost(userDetails, post);
        FileEntity file = fileRepository.findById(fileId).orElseThrow();
        file.setState(FileState.DELETED);
    }

    @Override
    public void deletePurchaseFile(UserDetails userDetails, UUID purchaseId, UUID fileId) {
        PurchaseEntity purchase = purchaseRepository.findById(purchaseId).orElseThrow(PurchaseNotFoundException::new);
        ownCheckPurchase(userDetails, purchase);
    }


    private void ownCheckPost(UserDetails userDetails, PostEntity post) {
        if (!post.getAuthor().getUsername()
                .equals(userDetails.getUsername())) {
            throw new PostForbiddenException();
        }
    }

    private void ownCheckPurchase(UserDetails userDetails, PurchaseEntity purchase) {
        String username = purchase.getCustomer().getUsername();
        List<String> friends = purchase.getGeneralPurchases().stream().map(s -> s.getCustomer().getUsername()).collect(Collectors.toList());
        if (!username.equals(userDetails.getUsername()) || friends.contains(userDetails.getUsername())) {
            throw new PurchaseForbiddenException();
        }
    }

    private FileEntity getFile(MultipartFile file) {
        return FileEntity.builder()
                .originalFileName(file.getOriginalFilename())
                .contentType(file.getContentType())
                .size(file.getSize())
                .build();
    }
}
