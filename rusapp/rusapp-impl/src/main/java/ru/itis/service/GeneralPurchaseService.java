package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreateGeneralPurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.GeneralPurchaseResponse;
import ru.itis.enums.State;

import java.util.List;
import java.util.UUID;

public interface GeneralPurchaseService {

    GeneralPurchaseResponse getGeneralPurchase(UUID purchaseId);

    List<GeneralPurchaseResponse> getUserGeneralPurchases(int page, int size, String username);

    void confirmGeneralPurchase(UserDetails userDetails, UUID generalPurchaseId, State state);

    GeneralPurchaseResponse addGeneralPurchase(UserDetails userDetails, CreateGeneralPurchaseRequest request);

    List<GeneralPurchaseResponse> searchGeneralPurchasesByCriteria(int page, int size, String username, String criteria);

    GeneralPurchaseResponse updatePurchase(UserDetails userDetails, UUID generalPurchaseId, UpdatePurchaseRequest request);
}
