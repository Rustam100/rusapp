package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.UpdateUserRequest;
import ru.itis.enums.Role;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.UserRequest;
import ru.itis.dto.response.UserResponse;
import ru.itis.enums.State;
import ru.itis.exception.FriendshipException;
import ru.itis.exception.user.FailureLoginException;
import ru.itis.exception.user.OccupiedEmailException;
import ru.itis.exception.user.OccupiedUsernameException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.FriendEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.FriendRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.UserService;
import ru.itis.utils.EmailUtil;
import ru.itis.utils.mapper.UserMapper;

import javax.transaction.Transactional;
import java.util.*;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final EmailUtil emailUtil;
    private final PasswordEncoder passwordEncoder;

    private final FriendRepository friendRepository;

    @Value("${name.mail.template}")
    private String emailTemplaName;

    @Override
    public UserEntity findBySubject(String subject) {
        return userRepository.findOneByUsername(subject)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserResponse login(UserRequest userRequest) {
        return userRepository.findOneByUsername(userRequest.getUsername())
                .filter(user -> passwordEncoder.matches(userRequest.getPassword(), user.getHashPassword()))
                .map(userMapper::toUserResponse)
                .orElseThrow(FailureLoginException::new);
    }

    @Override
    public UUID createUser(CreateUserRequest user) {
        Optional<UserEntity> userTestUsername = userRepository.findOneByUsername(user.getUsername());
        if (userTestUsername.isPresent()) {
            throw new OccupiedUsernameException("Username is occupied");
        }
        Optional<UserEntity> userTestEmail = userRepository.findOneByEmail(user.getEmail());
        if (userTestEmail.isPresent()) {
            throw new OccupiedEmailException("Email is occupied");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity userEntity = userMapper.toUserEntity(user);
        userEntity.setRole(Role.USER);
        userEntity.setState(State.NOT_CONFIRMED);
        UUID userId = userRepository.save(userEntity).getUuid();
        toSendConfirmLetterEmail(userEntity);
        return userId;
    }

    @Override
    public UserResponse getUserById(UUID userId) {
        return userMapper.toUserResponse(
                userRepository.findById(userId)
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    public UserResponse getUserByUsername(String username) {
        return userMapper.toUserResponse(
                userRepository.findOneByUsername(username)
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    public List<UserResponse> getAllUsers() {
        return userMapper.toUserResponseList(userRepository.findAll());
    }

    @Override
    public void deleteUser(UserDetails userDetails) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        user.setState(State.DELETE);
        userRepository.save(user);
    }

    @Override
    public UserResponse updateUser(UserDetails userDetails, UpdateUserRequest userRequest) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        userMapper.updateUserFromUpdateUserRequest(userRequest, user);
        user.setHashPassword(passwordEncoder.encode(user.getHashPassword()));
        userRepository.save(user);
        return userMapper.toUserResponse(user);
    }

    @Override
    public UserResponse getProfile(UserDetails userDetails) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        return userMapper.toUserResponse(user);
    }

    @Override
    public List<UserResponse> getActiveUsers() {
        return userMapper.toUserResponseList(userRepository.findAllByStateIs(State.CONFIRMED));
    }

    @Override
    public List<UserResponse> getUserFriends(String username) {
        UserEntity user = userRepository.findOneByUsername(username)
                .orElseThrow(UserNotFoundException::new);
        List<FriendEntity> friendEntity = friendRepository.findFriends(user.getUuid())
                .orElseThrow(() -> new FriendshipException("does not have friends"));
        List<UserEntity> resultList = new ArrayList<>();
        for (FriendEntity friend : friendEntity) {
            if (friend.getFriend().getUuid().equals(user.getUuid())) {
                resultList.add(friend.getRequester());
            } else if (friend.getRequester().getUuid().equals(user.getUuid())) {
                resultList.add(friend.getFriend());
            }
        }
        return userMapper.toUserResponseList(resultList);
    }

    @Transactional
    @Override
    public void confirmUser(UUID userId) {
        UserEntity user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        user.setState(State.CONFIRMED);
    }

    private void toSendConfirmLetterEmail(UserEntity newUser) {
        Runnable task = () -> {
            HashMap<String, String> data = new HashMap<>();
            data.put("confirm_code", newUser.getUuid().toString());
            data.put("name", newUser.getUsername());
            emailUtil.sendMail(newUser.getEmail(), "confirm", emailTemplaName,
                    data);
        };
        Thread thread = new Thread(task);
        thread.start();
    }
}