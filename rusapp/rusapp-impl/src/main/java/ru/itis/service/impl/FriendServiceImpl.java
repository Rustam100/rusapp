package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.UserResponse;
import ru.itis.exception.FriendshipException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.FriendEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.FriendRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.FriendService;
import ru.itis.utils.mapper.UserMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FriendServiceImpl implements FriendService {

    private final UserRepository userRepository;
    private final FriendRepository friendRepository;

    private final UserMapper userMapper;

    @Override
    public void sendFriendRequest(UserDetails userDetails, String friendUsername) {
        UserEntity friend = userRepository.findOneByUsername(friendUsername)
                .orElseThrow(UserNotFoundException::new);
        UserEntity requester = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        Optional<FriendEntity> friendEntity = friendRepository.
                findOneFromFriends(friend.getUuid(), requester.getUuid());
        if (friendEntity.isPresent()) {
            throw new FriendshipException("Friend request already exists");
        } else {
            FriendEntity friendShip = FriendEntity.builder()
                    .requester(requester)
                    .friend(friend)
                    .isActive(false)
                    .build();

            friendRepository.save(friendShip);

        }
    }

    @Override
    public void acceptFriendRequest(UserDetails userDetails, String requesterUsername, String action) {
        UserEntity requester = userRepository.findOneByUsername(requesterUsername)
                .orElseThrow(UserNotFoundException::new);
        UserEntity friend = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        FriendEntity friendEntity = friendRepository.
                findFriendsByFriendIdAndRequesterIdWhereActiveIsFalse(
                        friend.getUuid(),
                        requester.getUuid())
                .orElseThrow(() -> new FriendshipException("Friend's request does not exist"));
        if (action.equals("cancel")) {
                friendRepository.deleteById(friendEntity.getUuid());
        } else if (action.equals("accept")) {
            friendEntity.setActive(true);
            friendRepository.save(friendEntity);
        }
    }


    @Override
    public void deleteFriend(UserDetails userDetails, String username) {
        UserEntity deletedFriend = userRepository.findOneByUsername(username)
                .orElseThrow(UserNotFoundException::new);
        UserEntity deletingFriend = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        FriendEntity friendEntity = friendRepository.
                findFriendsByIDs(deletedFriend.getUuid(), deletingFriend.getUuid())
                .orElseThrow(() -> new FriendshipException("not found"));
        friendEntity.setActive(false);
        friendRepository.save(friendEntity);
    }

    @Override
    public List<UserResponse> getFriends(UserDetails userDetails, String section) {
        if (section != null) {
            switch (section.toLowerCase()) {
                case "all_requests":
                    return getRequests(userDetails);
                case "all":
                default:
                    return getAllFriends(userDetails);
            }

        } else {
            return getAllFriends(userDetails);
        }

    }

    @Override
    public List<UserResponse> getRequests(UserDetails userDetails) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        List<UserEntity> resultList = new ArrayList<>();
        List<FriendEntity> friends = friendRepository.findAllByFriendAndActiveIsFalse(user.getUuid())
                .orElseThrow(FriendshipException::new);
        for (FriendEntity friend : friends) {
            resultList.add(friend.getRequester());
        }
        return userMapper.toUserResponseList(resultList);
    }

    @Override
    public List<UserResponse> getAllFriends(UserDetails userDetails) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
        List<FriendEntity> friendEntity = friendRepository.findFriends(user.getUuid())
                .orElseThrow(() -> new FriendshipException("does not have friends"));
        List<UserEntity> resultList = new ArrayList<>();
        for (FriendEntity friend : friendEntity) {
            if (friend.getFriend().getUuid().equals(user.getUuid())) {
                resultList.add(friend.getRequester());
            } else if (friend.getRequester().getUuid().equals(user.getUuid())) {
                resultList.add(friend.getFriend());
            }
        }
        return userMapper.toUserResponseList(resultList);
    }




}
