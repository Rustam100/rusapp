package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreatePostRequest;
import ru.itis.dto.request.UpdatePostRequest;
import ru.itis.dto.response.PostResponse;

import java.util.List;
import java.util.UUID;

public interface PostService {

    PostResponse createPost(CreatePostRequest postRequest, UserDetails userDetails);

    List<PostResponse> getPosts(UserDetails userPrincipal, int page, int size);

    PostResponse getPostById(String postId);

    PostResponse updatePostById(UserDetails userDetails, UpdatePostRequest postRequest, String postId);

    void deletePostById(UserDetails userDetails, String postId);

    List<PostResponse> getUserPostsByUsername(String username, int page, int size);

    boolean checkPostByOwn(UUID authorId, UUID postId);

}
