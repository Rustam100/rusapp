package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreateGeneralPurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.GeneralPurchaseResponse;
import ru.itis.enums.State;
import ru.itis.exception.category.CategoryNotFoundException;
import ru.itis.exception.purchase.GeneralPurchaseNotFoundException;
import ru.itis.exception.purchase.PurchaseForbiddenException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.*;
import ru.itis.repository.CategoryRepository;
import ru.itis.repository.GeneralPurchaseRepository;
import ru.itis.repository.PurchaseRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.GeneralPurchaseService;
import ru.itis.specification.service.SpecificationService;
import ru.itis.utils.mapper.PurchaseMapper;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GeneralPurchaseServiceImpl implements GeneralPurchaseService {

    private final GeneralPurchaseRepository generalPurchaseRepository;
    private final UserRepository userRepository;
    private final PurchaseRepository purchaseRepository;
    private final CategoryRepository categoryRepository;
    private final PurchaseMapper purchaseMapper;
    private final SpecificationService specificationService;

    @Override
    public GeneralPurchaseResponse getGeneralPurchase(UUID purchaseId) {

        GeneralPurchaseEntity generalPurchase = generalPurchaseRepository.findById(purchaseId).orElseThrow(GeneralPurchaseNotFoundException::new);
        PurchaseEntity purchase = generalPurchase.getPurchase();
        List<String> friendsName = getFriendsNameByPurchase(generalPurchase.getPurchase());
        return purchaseMapper.toGeneralPurchaseResponse(purchase
                ,purchase.getCost()/ friendsName.size()
                ,friendsName);
    }

    @Override
    public List<GeneralPurchaseResponse> getUserGeneralPurchases(int page, int size, String username) {

        UserEntity user = getUser(username);

        List<GeneralPurchaseResponse> generalPurchaseResponses = new ArrayList<>();

        PageRequest request = PageRequest.of(page, size);

        List<GeneralPurchaseEntity> generalPurchases = generalPurchaseRepository.findByCustomerAndState(user, State.CONFIRMED, request)
                .orElseThrow(GeneralPurchaseNotFoundException::new).getContent();

        for (GeneralPurchaseEntity entities: generalPurchases) {

            List<String> friendName = getFriendsNameByPurchase(entities.getPurchase());
            friendName.remove(user.getUsername());

            PurchaseEntity purchase = entities.getPurchase();
            generalPurchaseResponses.add(purchaseMapper.toGeneralPurchaseResponse(
                    purchase
                    ,purchase.getCost()/friendName.size()
                    ,friendName));
        }
        return generalPurchaseResponses;
    }

    @Transactional
    @Override
    public void confirmGeneralPurchase(UserDetails userDetails, UUID generalPurchaseId, State state) {

        GeneralPurchaseEntity generalPurchase = getGeneralPurchaseEntity(userDetails, generalPurchaseId);
        if (state.equals(State.DELETE)){
            generalPurchaseRepository.delete(generalPurchase);
        } else {
            generalPurchase.setState(state);
            generalPurchase.setCreateDate(Instant.now());
        }
    }

    @Override
    public GeneralPurchaseResponse addGeneralPurchase(UserDetails userDetails, CreateGeneralPurchaseRequest request) {

        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername()).orElseThrow(UserNotFoundException::new);

        CategoryEntity category = categoryRepository.findByName(request.getCategoryTitle()).orElseThrow(CategoryNotFoundException::new);
        PurchaseEntity purchase = purchaseRepository.save(purchaseMapper.toPurchaseEntity(request, category));

        generalPurchaseRepository.save(new GeneralPurchaseEntity(user, purchase, State.CONFIRMED));

        for (String username: request.getFriends()) {
            UserEntity friend = userRepository.findOneByUsername(username).orElseThrow(UserNotFoundException::new);
            generalPurchaseRepository.save(new GeneralPurchaseEntity(friend, purchase, State.NOT_CONFIRMED));
        }
        return purchaseMapper.toGeneralPurchaseResponse(purchase, purchase.getCost()/request.getFriends().size(), request.getFriends());
    }

    @Override
    public List<GeneralPurchaseResponse> searchGeneralPurchasesByCriteria(int page, int size, String username, String criteria) {

        PageRequest request = PageRequest.of(page, size);
        Specification<PurchaseEntity> spec = null;

        if (!criteria.equals("none")) {spec = specificationService.getPurchaseEntitySpecification(criteria);}
        spec = specificationService.getGeneralPurchaseSpecification(username, spec);

        Page<PurchaseEntity> result = purchaseRepository.findAll(spec, request);

        List<GeneralPurchaseResponse> generalPurchases = new ArrayList<>();

        for (PurchaseEntity purchase: result.getContent()) {
            List<String> friendName = getFriendsNameByPurchase(purchase);
            generalPurchases.add(purchaseMapper.toGeneralPurchaseResponse(
                            purchase, purchase.getCost()/friendName.size(), friendName));
        }
        return generalPurchases;
    }

    @Transactional
    @Override
    public GeneralPurchaseResponse updatePurchase(UserDetails userDetails, UUID generalPurchaseId, UpdatePurchaseRequest request) {

        PurchaseEntity purchase = getGeneralPurchaseEntity(userDetails, generalPurchaseId).getPurchase();

        CategoryEntity category = categoryRepository.findByName(request.getCategoryTitle())
                .orElseThrow(CategoryNotFoundException::new);

        purchaseMapper.updatePurchase(purchase, request, category);

        List<String> friendsName = getFriendsNameByPurchase(purchase);
        return purchaseMapper.toGeneralPurchaseResponse(purchase
                ,purchase.getCost()/ friendsName.size()
                ,friendsName);
    }

    private UserEntity getUser(String username) {
        return userRepository.findOneByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    private List<String> getFriendsNameByPurchase(PurchaseEntity purchase) {
        return purchase.getGeneralPurchases().stream()
                .filter(s -> s.getState().equals(State.CONFIRMED))
                .map(s->s.getCustomer().getUsername())
                .collect(Collectors.toList());
    }

    private GeneralPurchaseEntity getGeneralPurchaseEntity(UserDetails userDetails, UUID generalPurchaseId) {
        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);

        GeneralPurchaseEntity generalPurchase = generalPurchaseRepository.findById(generalPurchaseId)
                .orElseThrow(GeneralPurchaseNotFoundException::new);

        if (!generalPurchase.getCustomer().equals(user)){
            throw new PurchaseForbiddenException();
        }
        return generalPurchase;
    }
}
