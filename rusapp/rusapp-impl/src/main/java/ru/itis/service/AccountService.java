package ru.itis.service;

import ru.itis.model.UserEntity;


public interface AccountService {

    UserEntity findBySubject(String subject);
}
