package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreatePurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.PurchaseResponse;
import ru.itis.exception.category.CategoryNotFoundException;
import ru.itis.exception.purchase.PurchaseForbiddenException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.exception.purchase.PurchaseNotFoundException;
import ru.itis.model.CategoryEntity;
import ru.itis.model.PurchaseEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.CategoryRepository;
import ru.itis.repository.PurchaseRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.PurchaseService;
import ru.itis.specification.service.SpecificationService;
import ru.itis.utils.mapper.PurchaseMapper;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
public class PurchaseServiceImpl implements PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;

    private final PurchaseMapper purchaseMapper;

    private final SpecificationService specificationService;

    @Override
    public PurchaseResponse getPurchase(UUID purchaseId) {
        return purchaseMapper.toPurchaseResponse(purchaseRepository.findById(purchaseId)
                .orElseThrow(PurchaseNotFoundException::new));
    }

    @Override
    public List<PurchaseResponse> getUserPurchases(String username, int page, int size) {

        UserEntity user = userRepository.findOneByUsername(username)
                .orElseThrow(UserNotFoundException::new);

        PageRequest request = PageRequest.of(page, size);
        Page<PurchaseEntity> result = purchaseRepository.findByCustomer(user, request).orElseThrow(PurchaseNotFoundException::new);
        return purchaseMapper.toPurchaseResponseList(result.getContent());
    }

    @Override
    public PurchaseResponse addNewPurchase(UserDetails userDetails, CreatePurchaseRequest createPurchaseRequest) {

        UserEntity user = userRepository.findOneByUsername(userDetails.getUsername()).orElseThrow(UserNotFoundException::new);
        CategoryEntity category = categoryRepository.findByName(createPurchaseRequest.getCategoryTitle()).orElseThrow(CategoryNotFoundException::new);

        PurchaseEntity purchase = purchaseMapper.toPurchaseEntity(createPurchaseRequest);
        purchase.setCustomer(user);
        purchase.setCategory(category);

        return purchaseMapper.toPurchaseResponse(
                purchaseRepository.save(purchase));
    }

    @Transactional
    @Override
    public PurchaseResponse updatePurchase(UserDetails userDetails, UUID purchaseId, UpdatePurchaseRequest request) {
        PurchaseEntity purchase = ownerShipCheckPurchase(userDetails, purchaseId);
        CategoryEntity category = categoryRepository.findByName(request.getCategoryTitle()).orElseThrow(CategoryNotFoundException::new);
        purchaseMapper.updatePurchase(purchase, request, category);
        return purchaseMapper.toPurchaseResponse(purchase);
    }

    @Override
    public void deletePurchase(UserDetails userDetails, UUID purchaseId) {
        ownerShipCheckPurchase(userDetails, purchaseId);
        purchaseRepository.deleteById(purchaseId);
    }

    @Override
    public List<PurchaseResponse> searchPurchasesByCriteria(int page, int size, String criteria) {

        PageRequest request = PageRequest.of(page, size);
        Page<PurchaseEntity> result;

        if (!criteria.equals("none")){
            Specification<PurchaseEntity> spec = specificationService.getPurchaseEntitySpecification(criteria);
            result = purchaseRepository.findAll(spec, request);
        } else {
            result = purchaseRepository.findAllByCustomerIsNotNull(request).orElseThrow(PurchaseNotFoundException::new);
        }
        return purchaseMapper.toPurchaseResponseList(result.getContent());
    }


    private PurchaseEntity ownerShipCheckPurchase(UserDetails userDetails, UUID purchaseId) {
        PurchaseEntity purchase = purchaseRepository.findById(purchaseId)
                .orElseThrow(PurchaseNotFoundException::new);
        if (purchase.getCustomer() == null) {
            throw new PurchaseForbiddenException();
        }
        if (!purchase.getCustomer().getUsername().equals(userDetails.getUsername())){
            throw new PurchaseForbiddenException();
        }
        return purchase;
    }

}
