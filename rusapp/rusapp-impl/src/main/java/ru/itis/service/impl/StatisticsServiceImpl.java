package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.PurchaseStatisticByCategory;
import ru.itis.dto.response.PurchaseStatisticResponse;
import ru.itis.exception.purchase.GeneralPurchaseNotFoundException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.UserEntity;
import ru.itis.repository.GeneralPurchaseRepository;
import ru.itis.repository.PurchaseRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.StatisticsService;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final PurchaseRepository purchaseRepository;
    private final GeneralPurchaseRepository generalPurchaseRepository;
    private final UserRepository userRepository;

    @Override
    public PurchaseStatisticResponse geGeneralPurchaseStatistics(String username, String from, String to) {
        UserEntity user = userRepository.findOneByUsername(username).orElseThrow(UserNotFoundException::new);
        List<UUID> purchases = generalPurchaseRepository.findByCustomer(user).orElseThrow(GeneralPurchaseNotFoundException::new)
                .stream().map(s-> s.getPurchase().getUuid()).collect(Collectors.toList());
        List<Object[]> list = purchaseRepository.findAllGroupByCategoryAndPurchases(checkToTime(to), checkFromTime(from), purchases);
        return getObject(list);
    }

    @Override
    public PurchaseStatisticResponse getPurchaseStatistics(String username, String from, String to) {
    List<Object[]> list = purchaseRepository.findAllGroupByCategory(username, checkToTime(to), checkFromTime(from));
        return getObject(list);
    }

    private PurchaseStatisticResponse getObject(List<Object[]> list) {
        List<PurchaseStatisticByCategory> statistic = new ArrayList<>();
        Double totalCost = 0.0;
        Long totalCount = 0L;
        for (Object[] o: list) {
            statistic.add(PurchaseStatisticByCategory.builder()
                    .category((String) o[0])
                    .sum((Double) o[1])
                    .count((Long) o[2])
                    .build());
            totalCost += (Double) o[1];
            totalCount += (Long) o[2];
        }

        return PurchaseStatisticResponse.builder()
                .totalCount(totalCount)
                .totalSum(totalCost)
                .purchaseStatisticByCategories(statistic)
                .build();
    }

    private Instant checkFromTime(String from) {
        if (from.equals("none")){
            from = Instant.EPOCH.toString();
        }
        return Instant.parse(from);
    }

    private Instant checkToTime(String to) {
        if (to.equals("none")){
            to = Instant.now().toString();
        }
        return Instant.parse(to);
    }


}
