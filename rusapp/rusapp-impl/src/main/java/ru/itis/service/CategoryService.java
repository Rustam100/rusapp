package ru.itis.service;

import ru.itis.model.CategoryEntity;

public interface CategoryService {

    CategoryEntity getCategory(String title);
}
