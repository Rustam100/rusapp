package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.UpdateUserRequest;
import ru.itis.dto.request.UserRequest;
import ru.itis.dto.response.UserResponse;

import java.util.List;
import java.util.UUID;

public interface UserService extends AccountService {

    UserResponse login(UserRequest request);

    UUID createUser(CreateUserRequest user);

    UserResponse getUserById(UUID userId);

    UserResponse getUserByUsername(String username);

    List<UserResponse> getAllUsers();

    void deleteUser(UserDetails userDetails);

    UserResponse updateUser(UserDetails userDetails,
                            UpdateUserRequest userRequest);

    UserResponse getProfile(UserDetails userDetails);

    List<UserResponse> getActiveUsers();

    List<UserResponse> getUserFriends(String username);

    void confirmUser(UUID userId);
}
