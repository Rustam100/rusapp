package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.enums.State;
import ru.itis.model.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    Optional<UserEntity> findOneByUsername(String username);

    Optional<List<UserEntity>> findByUsernameIsLike(String username);

    List<UserEntity> findAllByStateIs(State state);

    Optional<UserEntity> findOneByEmail(String email);
}
