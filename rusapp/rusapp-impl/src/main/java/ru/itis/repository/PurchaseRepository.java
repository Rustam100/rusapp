package ru.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.model.PurchaseEntity;
import ru.itis.model.UserEntity;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PurchaseRepository extends JpaRepository<PurchaseEntity, UUID>, JpaSpecificationExecutor<PurchaseEntity> {

    Optional<Page<PurchaseEntity>> findByCustomer(UserEntity user, Pageable pageable);

    Optional<Page<PurchaseEntity>> findAllByCustomerIsNotNull(Pageable pageable);

    @Query("select p.category.name, sum(p.cost) as sum_cost, count(p) as count_purchase from PurchaseEntity p where p.createDate > :from and p.createDate < :to and p.uuid in :purchasesId group by p.category.name order by sum(p.cost)")
    List<Object[]> findAllGroupByCategoryAndPurchases(@Param("to") Instant to,
                                                      @Param("from") Instant from,
                                                      @Param("purchasesId") List<UUID> purchasesId);

    @Query("select p.category.name, sum(p.cost) as sum_cost, count(p) as count_purchase from PurchaseEntity p where p.createDate > :from and p.createDate < :to and p.customer.username = :username group by p.category.name order by sum(p.cost)")
    List<Object[]> findAllGroupByCategory(@Param("username") String username,
                                          @Param("to") Instant to,
                                          @Param("from") Instant from);
}
