package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.model.FriendEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FriendRepository extends JpaRepository<FriendEntity, UUID> {

    @Query(nativeQuery = true, value = "select * from friends where is_active is false " +
            "and (friend_id = :friend and requester_id = :requester)")
    Optional<FriendEntity> findFriendsByFriendIdAndRequesterIdWhereActiveIsFalse(
            @Param("friend") UUID friend,
            @Param("requester") UUID requester);

    @Query(nativeQuery = true,
            value = "select * from friends where requester_id = :requester and is_active is true")
    Optional<List<FriendEntity>> findFriendsByRequester(@Param("requester") UUID requester);

    @Query(nativeQuery = true, value = "select * from friends where friend_id = :friend and is_active is true")
    Optional<List<FriendEntity>> findFriendsByFriend(@Param("friend") UUID friend);

    @Query(nativeQuery = true, value = "select * from friends where " +
            "(friend_id = :friend and requester_id = :requester) " +
            "or " +
            "(friend_id = :requester and requester_id = :friend)")
    Optional<FriendEntity> findOneFromFriends(@Param("friend") UUID friend, @Param("requester") UUID requester);

    @Query(nativeQuery = true, value = "select * from friends " +
            "where (friend_id = :id or requester_id = :id) and is_active is true")
    Optional<List<FriendEntity>> findFriends(@Param("id") UUID id);

    @Query(nativeQuery = true, value = "select * from friends where friend_id = :friend and is_active is false")
    Optional<List<FriendEntity>> findAllByFriendAndActiveIsFalse(@Param("friend") UUID friend);

    @Query(nativeQuery = true, value = "select * from friends where is_active is true " +
            "and" +
            "((friend_id = :deleted and requester_id = :deleting)" +
            "or" +
            "(friend_id = :deleting and requester_id = :deleted))")
    Optional<FriendEntity> findFriendsByIDs(@Param("deleted") UUID deleted, @Param("deleting") UUID deleting);




}
