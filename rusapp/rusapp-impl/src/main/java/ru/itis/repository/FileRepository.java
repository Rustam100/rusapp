package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.enums.FileState;
import ru.itis.model.FileEntity;

import java.util.Optional;
import java.util.UUID;

public interface FileRepository extends JpaRepository<FileEntity, UUID> {
    Optional<FileEntity> findByUuidAndState(UUID uuid, FileState state);
}
