package ru.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.enums.State;
import ru.itis.model.GeneralPurchaseEntity;
import ru.itis.model.PurchaseEntity;
import ru.itis.model.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GeneralPurchaseRepository extends JpaRepository<GeneralPurchaseEntity, UUID> {

    Optional<List<GeneralPurchaseEntity>> findByCustomer(UserEntity user);

    Optional<GeneralPurchaseEntity> findByCustomerAndPurchase(UserEntity user, PurchaseEntity purchase);

    Optional<List<GeneralPurchaseEntity>> findAllByCustomerIn(List<UserEntity> users);

    Optional<Page<GeneralPurchaseEntity>> findByCustomerAndState(UserEntity user, State state, Pageable pageable);

}
