package ru.itis.utils.mapper;

import org.mapstruct.Mapper;
import ru.itis.dto.response.FileResponse;
import ru.itis.model.FileEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileMapper {

    FileResponse toFileResponse(FileEntity fileEntity);

    List<FileResponse> toFileResponseList(List<FileEntity> files);
}
