package ru.itis.utils;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class EmailUtil {

    private final JavaMailSender mailSender;
    private final Configuration configuration;
    public static final String LINK_PREFIX = "localhost/api/rusapp/users/confirm?code=";

    @Value("${mail.username}")
    private String from;


    public void sendMail(String to, String subject, String templateName, Map<String, String> data) {

        MimeMessagePreparator preparator = null;
        try {
            try {
                String confirmLink = LINK_PREFIX + data.get("confirm_code");
                data.put("confirm_code", confirmLink);
                final String mailText = FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(templateName, "UTF-8"), data);
                preparator = mimeMessage -> {
                    MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                    messageHelper.setSubject(subject);
                    messageHelper.setText(mailText, true);
                    messageHelper.setTo(to);
                    messageHelper.setFrom(from);
                };
                mailSender.send(preparator);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
