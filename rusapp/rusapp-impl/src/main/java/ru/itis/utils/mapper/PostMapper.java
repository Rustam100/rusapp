package ru.itis.utils.mapper;

import org.mapstruct.*;
import ru.itis.dto.request.CreatePostRequest;
import ru.itis.dto.request.PostRequest;
import ru.itis.dto.request.UpdatePostRequest;
import ru.itis.dto.response.PostResponse;
import ru.itis.model.PostEntity;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class})
public interface PostMapper {

    PostEntity toPostEntityFromPostRequest(PostRequest postRequest);

    PostEntity toPostEntityFromCreateRequest(CreatePostRequest createPostRequest);

    PostResponse toPostResponse(PostEntity postEntity);

    List<PostResponse> toPostResponse(List<PostEntity> posts);


    @BeanMapping(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
            nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updatePost(@MappingTarget PostEntity post, UpdatePostRequest postRequest);

}
