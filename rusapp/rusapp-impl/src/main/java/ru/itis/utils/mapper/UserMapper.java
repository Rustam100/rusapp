package ru.itis.utils.mapper;

import org.mapstruct.*;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.UpdateUserRequest;
import ru.itis.dto.request.UserRequest;
import ru.itis.dto.response.UserResponse;
import ru.itis.model.UserEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(source = "password", target = "hashPassword")
    UserEntity toUserEntity(UserRequest userRequest);

    @Mapping(source = "password", target = "hashPassword")
    UserEntity toUserEntity(CreateUserRequest createUserRequest);

    @Mapping(source = "password", target = "hashPassword")
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateUserFromUpdateUserRequest(UpdateUserRequest updateUserRequest, @MappingTarget UserEntity user);

    UserResponse toUserResponse(UserEntity user);

    List<UserEntity> toUsers(List<UserRequest> userRequests);

    List<UserEntity> toUserEntityList(List<CreateUserRequest> createUserRequests);

    List<UserResponse> toUserResponseList(List<UserEntity> users);
}
