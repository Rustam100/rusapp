package ru.itis.utils.mapper;

import org.mapstruct.Mapper;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.model.CategoryEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {

    CategoryResponse toCategoryResponse(CategoryEntity categoryEntity);

    List<CategoryResponse> toCategoryResponse(List<CategoryEntity> categories);
}
