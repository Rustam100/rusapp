package ru.itis.utils.mapper;

import org.mapstruct.*;
import ru.itis.dto.request.CreateGeneralPurchaseRequest;
import ru.itis.dto.request.CreatePurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.GeneralPurchaseResponse;
import ru.itis.dto.response.PurchaseResponse;
import ru.itis.model.CategoryEntity;
import ru.itis.model.PurchaseEntity;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CategoryMapper.class, FileMapper.class})
public interface PurchaseMapper {

    PurchaseEntity toPurchaseEntity(CreatePurchaseRequest createPurchaseRequest);

    PurchaseResponse toPurchaseResponse(PurchaseEntity purchase);

    @BeanMapping(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
                nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "category", target = "purchase.category")
    @Mapping(target = "uuid", ignore = true)
    void updatePurchase(@MappingTarget PurchaseEntity purchase, UpdatePurchaseRequest purchaseRequest, CategoryEntity category);

    List<PurchaseResponse> toPurchaseResponseList(List<PurchaseEntity> purchases);

    @Mapping(source = "purchase.cost", target = "generalCost")
    @Mapping(source = "cost",target = "cost")
    GeneralPurchaseResponse toGeneralPurchaseResponse(PurchaseEntity purchase, Double cost, List<String> friends);

    @Mapping(source = "category", target = "category")
    @Mapping(target = "uuid", ignore = true)
    PurchaseEntity toPurchaseEntity(CreateGeneralPurchaseRequest createGeneralPurchaseRequest, CategoryEntity category);

}
