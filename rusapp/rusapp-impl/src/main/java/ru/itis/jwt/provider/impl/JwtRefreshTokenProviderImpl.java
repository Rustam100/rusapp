package ru.itis.jwt.provider.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.dto.response.UserResponse;
import ru.itis.exception.user.TokenRefreshException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.jwt.provider.JwtRefreshTokenProvider;
import ru.itis.model.RefreshTokenEntity;
import ru.itis.model.UserRefreshTokenEntity;
import ru.itis.repository.UserRefreshTokenRepository;
import ru.itis.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class JwtRefreshTokenProviderImpl implements JwtRefreshTokenProvider {


    @Value("${jwt.expiration.refresh.mills}")
    private Long expirationRefreshInMills;

    private final UserRefreshTokenRepository refreshTokenRepository;
    private final UserRepository userRepository;

    @Transactional
    @Override
    public UUID generateRefreshToken(UserResponse accountResponse) {
        UserRefreshTokenEntity refreshToken = UserRefreshTokenEntity.builder()
                .account(userRepository
                        .findOneByUsername(accountResponse.getUsername())
                        .orElseThrow(UserNotFoundException::new))
                .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                .build();
        refreshTokenRepository.save(refreshToken);
        return refreshToken.getUuid();
    }

    @Override
    public RefreshTokenEntity verifyRefreshTokenExpiration(UUID refreshToken) {

        UserRefreshTokenEntity token = refreshTokenRepository.findById(refreshToken).orElseThrow((Supplier<RuntimeException>) ()
                -> new TokenRefreshException(refreshToken.toString(), "Токен не существует."));

        refreshTokenRepository.delete(token);

        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            throw new TokenRefreshException(String.valueOf(token.getUuid()), "Срок действия токена обновления истек.");
        }

        return refreshTokenRepository.save(
                UserRefreshTokenEntity.builder()
                        .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                        .account(token.getAccount())
                        .build());
    }
}
