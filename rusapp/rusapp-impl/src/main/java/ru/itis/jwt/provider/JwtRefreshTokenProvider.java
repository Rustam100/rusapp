package ru.itis.jwt.provider;

import ru.itis.dto.response.UserResponse;
import ru.itis.model.RefreshTokenEntity;

import java.util.UUID;

public interface JwtRefreshTokenProvider {

    UUID generateRefreshToken(UserResponse accountResponse);

    RefreshTokenEntity verifyRefreshTokenExpiration(UUID refreshToken);
}
