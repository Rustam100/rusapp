package ru.itis.jwt.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.dto.response.UserResponse;
import ru.itis.jwt.provider.JwtAccessTokenProvider;
import ru.itis.jwt.provider.JwtRefreshTokenProvider;
import ru.itis.jwt.service.JwtTokenService;
import ru.itis.model.RefreshTokenEntity;
import ru.itis.model.UserEntity;

import java.util.Collections;
import java.util.UUID;

import static ru.itis.consts.RusappConstants.ROLE;

@Service
@RequiredArgsConstructor
public class JwtTokenServiceImpl implements JwtTokenService {

    public static final String BEARER = "Bearer ";
    private final JwtAccessTokenProvider jwtAccessTokenProvider;
    private final JwtRefreshTokenProvider jwtRefreshTokenProvider;

    @Override
    public boolean tokenIsValid(String token) {
        return jwtAccessTokenProvider.validateAccessToken(token);
    }

    @Override
    public UserEntity getUserByToken(String token) {
        return jwtAccessTokenProvider.getUserByToken(token);
    }

    @Override
    public TokenCoupleResponse generateTokenCouple(UserResponse accountResponse) {
        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                accountResponse.getUsername(),
                Collections.singletonMap(ROLE, accountResponse.getRole().toString())
        );
        UUID refreshToken = jwtRefreshTokenProvider.generateRefreshToken(accountResponse);
        return TokenCoupleResponse.builder()
                .accessToken(BEARER.concat(accessToken))
                .refreshToken(refreshToken)
                .accessTokenExpirationDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken))
                .build();
    }

    @Override
    public TokenCoupleResponse refreshAccessToken(TokenCoupleResponse tokenCoupleResponse) {

        String role = jwtAccessTokenProvider.getRoleFromAccessToken(tokenCoupleResponse.getAccessToken().substring(BEARER.length()));

        RefreshTokenEntity verifiedRefreshToken = jwtRefreshTokenProvider.verifyRefreshTokenExpiration(tokenCoupleResponse.getRefreshToken());

        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                jwtAccessTokenProvider.getSubjectFromAccessToken(tokenCoupleResponse.getAccessToken().substring(BEARER.length())),
                Collections.singletonMap(ROLE, role));

        return TokenCoupleResponse.builder()
                .refreshToken(verifiedRefreshToken.getUuid())
                .accessToken(BEARER.concat(accessToken))
                .accessTokenExpirationDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken))
                .build();

    }
}
