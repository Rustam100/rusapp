package ru.itis.jwt.service;

import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.dto.response.UserResponse;
import ru.itis.model.UserEntity;

public interface JwtTokenService {

    boolean tokenIsValid(String token);

    UserEntity getUserByToken(String token);

    TokenCoupleResponse generateTokenCouple(UserResponse accountResponse);

    TokenCoupleResponse refreshAccessToken(TokenCoupleResponse tokenCoupleResponse);
}
