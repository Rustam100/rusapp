package ru.itis.security.userdetails;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import ru.itis.enums.State;
import ru.itis.model.UserEntity;

@Slf4j
@RequiredArgsConstructor
@Service
public class AccountUserDetailService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken) throws UsernameNotFoundException {
        UserEntity user = (UserEntity) preAuthenticatedAuthenticationToken.getPrincipal();
        return User.builder()
                .username(user.getUsername())
                .password(passwordEncoder.encode(user.getHashPassword()))
                .authorities(user.getRole().toString())
                .accountLocked(user.getState().equals(State.BAN))
                .accountExpired(user.getState().equals(State.DELETE))
                .build();
    }
}
