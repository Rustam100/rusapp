package ru.itis.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import ru.itis.exception.user.AuthenticationHeaderException;
import ru.itis.jwt.service.JwtTokenService;
import ru.itis.model.UserEntity;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RequiredArgsConstructor
public class TokenAuthenticationFilter extends RequestHeaderAuthenticationFilter {

    private final JwtTokenService jwtTokenService;
    private final ObjectMapper objectMapper;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService, ObjectMapper objectMapper) {
        this.jwtTokenService = jwtTokenService;
        this.objectMapper = objectMapper;
        this.setAuthenticationManager(authenticationManager);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            Optional<String> token = Optional.ofNullable(getTokenFromHeader(((HttpServletRequest) request).getHeader(AUTHORIZATION)));
            if (token.isPresent()){
                if  (jwtTokenService.tokenIsValid(token.get())){
                    UserEntity user = jwtTokenService.getUserByToken(token.get());
                    PreAuthenticatedAuthenticationToken authenticationToken = new PreAuthenticatedAuthenticationToken(user, token);

                    if (Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    } else if (!SecurityContextHolder.getContext().getAuthentication().getCredentials().equals(token)) {
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }
            }
        } catch (Exception e) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            ((HttpServletResponse)response).setStatus(HttpServletResponse.SC_FORBIDDEN);
            objectMapper.writeValue(response.getWriter(), Collections.singletonMap("Error", "Token is expired or invalid"));
        }
        chain.doFilter(request, response);
    }

    private String getTokenFromHeader(String header) {
        if (header == null) {
            return null;
        }
        if (!header.startsWith("Bearer ")) {
            throw new AuthenticationHeaderException("Invalid authentication scheme found in Authorization header");
        }
        String token = header.substring("Bearer ".length());
        if (token.isEmpty()) {
            throw new IllegalArgumentException("Authorization header token is empty");
        }
        return token;
    }
}
