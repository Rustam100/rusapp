package ru.itis.specification.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.SearchPurchasesCriteria;
import ru.itis.exception.category.CategoryNotFoundException;
import ru.itis.exception.purchase.GeneralPurchaseNotFoundException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.GeneralPurchaseEntity;
import ru.itis.model.PurchaseEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.CategoryRepository;
import ru.itis.repository.GeneralPurchaseRepository;
import ru.itis.repository.UserRepository;
import ru.itis.specification.PurchaseSpecificationsBuilder;
import ru.itis.specification.service.SpecificationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static ru.itis.constants.PurchaseConstant.*;

@Service
@RequiredArgsConstructor
public class PurchaseSpecificationServiceImpl implements SpecificationService {

    private final CategoryRepository categoryRepository;
    private final UserRepository userRepository;
    private final GeneralPurchaseRepository generalPurchaseRepository;

    @Override
    public Specification<PurchaseEntity> getPurchaseEntitySpecification(String criteria) {
        PurchaseSpecificationsBuilder builder = new PurchaseSpecificationsBuilder();
        Arrays.stream(criteria.split(","))
                .map(s -> {
                    List<String> params = Arrays.stream(s.substring(1).split("=")).collect(Collectors.toList());
                    return new SearchPurchasesCriteria(s.substring(0,1), params.get(0), params.get(1));
                })
                .forEach(s -> {
                    switch (s.getKey()){
                        case "customer":
                            if (s.getOperation().equals(EQUAL)){
                                builder.with(EQUAL, s.getKey(),
                                        userRepository.findOneByUsername((String)s.getValue()).orElseThrow(UserNotFoundException::new));
                            } else if (s.getOperation().equals(LIKE)){
                                builder.with(IN, s.getKey(),
                                        userRepository.findByUsernameIsLike(s.getValue() + "%").orElseThrow(UserNotFoundException::new));
                            }
                            break;
                        case "category":
                            if (s.getOperation().equals(EQUAL)){
                                builder.with(EQUAL, s.getKey(),
                                        categoryRepository.findByName((String) s.getValue()).orElseThrow(CategoryNotFoundException::new));
                            } else if (s.getOperation().equals(LIKE)){
                                builder.with(IN, s.getKey(),
                                        categoryRepository.findByNameIsLike(s.getValue() + "%").orElseThrow(CategoryNotFoundException::new));
                            }
                            break;
                        default:
                            builder.with(s.getOperation(), s.getKey(), s.getValue());
                    }
                });
        return builder.build();
    }

    @Override
    public Specification<PurchaseEntity> getGeneralPurchaseSpecification(String username, Specification<PurchaseEntity> specification) {

        PurchaseSpecificationsBuilder builder = new PurchaseSpecificationsBuilder();

        List<GeneralPurchaseEntity> generalPurchases = new ArrayList<>();

        if (username.substring(0,1).equals(LIKE)){

            List<UserEntity> users = userRepository.findByUsernameIsLike(username.substring(1)+"%")
                    .orElseThrow(UserNotFoundException::new);
            generalPurchases.addAll(generalPurchaseRepository.findAllByCustomerIn(users)
                    .orElseThrow(GeneralPurchaseNotFoundException::new));

        } else if (username.substring(0,1).equals(EQUAL)) {

            UserEntity user = userRepository.findOneByUsername(username.substring(1))
                    .orElseThrow(UserNotFoundException::new);
            generalPurchases.addAll(generalPurchaseRepository.findByCustomer(user)
                    .orElseThrow(GeneralPurchaseNotFoundException::new));
        } else {
            generalPurchases.addAll(generalPurchaseRepository.findAll());
        }

        List<UUID> purchaseId = generalPurchases.stream().map(s->s.getPurchase().getUuid()).distinct().collect(Collectors.toList());

        if (specification == null) {
            return Specification.where(builder.with(IN,"uuid",purchaseId).build());
        }
        return specification.and(builder.with(IN,"uuid",purchaseId).build());
    }
}
