package ru.itis.specification;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import ru.itis.dto.request.SearchPurchasesCriteria;
import ru.itis.model.PurchaseEntity;

import javax.persistence.criteria.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static ru.itis.constants.PurchaseConstant.*;

@AllArgsConstructor
public class PurchaseSpecification implements Specification<PurchaseEntity> {

    private SearchPurchasesCriteria purchasesCriteria;

    @Override
    public Predicate toPredicate(Root<PurchaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        switch (purchasesCriteria.getOperation()) {
            case LIKE: {
                purchasesCriteria.setValue("%" + purchasesCriteria.getValue() + "%");
                return criteriaBuilder.like(
                        root.get(purchasesCriteria.getKey()), purchasesCriteria.getValue().toString());
            }
            case LESS_THEN: {
                if (purchasesCriteria.getKey().equals("createDate")) {
                    LocalDate date = LocalDate.parse((CharSequence) purchasesCriteria.getValue());
                    Instant instant = date.atStartOfDay(ZoneId.systemDefault()).toInstant();
                    return criteriaBuilder.lessThanOrEqualTo(
                            root.get(purchasesCriteria.getKey()), instant);
                } else {
                    return criteriaBuilder.lessThanOrEqualTo(
                            root.get(purchasesCriteria.getKey()), purchasesCriteria.getValue().toString());
                }
            }
            case GREATER_THEN: {
                if (purchasesCriteria.getKey().equals("createDate")) {
                    LocalDate date = LocalDate.parse((CharSequence) purchasesCriteria.getValue());
                    Instant instant = date.atStartOfDay(ZoneId.systemDefault()).toInstant();
                    return criteriaBuilder.greaterThanOrEqualTo(
                            root.get(purchasesCriteria.getKey()), instant);
                } else {
                    return criteriaBuilder.greaterThanOrEqualTo(
                            root.get(purchasesCriteria.getKey()), purchasesCriteria.getValue().toString());
                }
            }
            case IN: {
                return criteriaBuilder.in(
                        root.get(purchasesCriteria.getKey())).value(purchasesCriteria.getValue());
            }
            case EQUAL: {
                return criteriaBuilder.equal(
                        root.get(purchasesCriteria.getKey()), purchasesCriteria.getValue());
            }
        }
        return null;
    }
}
