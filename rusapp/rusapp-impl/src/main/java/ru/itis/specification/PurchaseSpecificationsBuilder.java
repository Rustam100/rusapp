package ru.itis.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.itis.dto.request.SearchPurchasesCriteria;
import ru.itis.model.PurchaseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class PurchaseSpecificationsBuilder {

    private final List<SearchPurchasesCriteria> params;

    public PurchaseSpecificationsBuilder() {
        params = new ArrayList<>();
    }

    public PurchaseSpecificationsBuilder with(String operation, String key, Object value) {
        params.add(new SearchPurchasesCriteria(operation, key, value));
        return this;
    }

    public Specification<PurchaseEntity> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification> specs = params.stream()
                .map(PurchaseSpecification::new)
                .collect(Collectors.toList());

        Specification result = specs.get(0);
        for (int i = 1; i < params.size(); i++) {
            result = Specification.where(result).and(specs.get(i));
        }
        return result;
    }
}
