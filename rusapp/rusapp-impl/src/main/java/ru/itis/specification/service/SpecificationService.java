package ru.itis.specification.service;

import org.springframework.data.jpa.domain.Specification;
import ru.itis.model.PurchaseEntity;

public interface SpecificationService {

    Specification<PurchaseEntity> getPurchaseEntitySpecification(String criteria);

    Specification<PurchaseEntity> getGeneralPurchaseSpecification(String username, Specification<PurchaseEntity> specification);
}
