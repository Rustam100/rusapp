package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.api.FileApi;
import ru.itis.dto.response.FileResponse;
import ru.itis.service.FileService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class FileController implements FileApi<UserDetails> {

    private final FileService fileService;

    @Override
    public FileResponse getFile(UUID fileId) {
        return fileService.getFile(fileId);
    }

    @Override
    public List<FileResponse> uploadToPost(UserDetails userDetails, UUID postId, MultipartFile[] multipartFiles) {
        return fileService.uploadToPost(userDetails, postId, multipartFiles);
    }

    @Override
    public List<FileResponse> uploadToPurchase(UserDetails userDetails, UUID purchaseId, MultipartFile[] multipartFiles) {
        return fileService.uploadToPurchase(userDetails, purchaseId, multipartFiles);
    }

    @Override
    public void deletePostFile(UserDetails userDetails, UUID postId, UUID fileId) {
        fileService.deletePostFile(userDetails, postId, fileId);
    }

    @Override
    public void deletePurchaseFile(UserDetails userDetails, UUID purchaseId, UUID fileId) {
        fileService.deletePurchaseFile(userDetails, purchaseId, fileId);
    }
}
