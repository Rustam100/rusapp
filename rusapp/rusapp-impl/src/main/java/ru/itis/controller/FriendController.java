package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.FriendApi;
import ru.itis.dto.response.UserResponse;
import ru.itis.service.FriendService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class FriendController implements FriendApi<UserDetails> {

    private final FriendService friendService;

    @Override
    public List<UserResponse> getFriends(UserDetails userDetails,
                                         String section) {
        return friendService.getFriends(userDetails, section);
    }

    @Override
    public void sendRequest(UserDetails userDetails,
                            String friendUsername) {
        friendService.sendFriendRequest(userDetails, friendUsername);
    }

    @Override
    public void acceptRequest(UserDetails userDetails,
                              String requesterUsername,
                              String action) {
        friendService.acceptFriendRequest(userDetails, requesterUsername, action);
    }

    @Override
    public void deleteFriend(UserDetails userDetails,
                             String username) {
        friendService.deleteFriend(userDetails, username);
    }
}
