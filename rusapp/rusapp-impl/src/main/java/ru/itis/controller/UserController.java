package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.UserApi;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.UpdateUserRequest;
import ru.itis.dto.request.UserRequest;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.dto.response.UserResponse;
import ru.itis.jwt.service.JwtTokenService;
import ru.itis.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@Slf4j
public class UserController implements UserApi<UserDetails> {

    private final UserService userService;
    private final JwtTokenService tokenService;

    @Override
    public TokenCoupleResponse login(UserRequest userRequest) {
        return tokenService.generateTokenCouple(userService.login(userRequest));
    }

    @Override
    public UUID createUser(@Valid CreateUserRequest user) {
        return userService.createUser(user);
    }

    @Override
    public UserResponse getProfile(UserDetails userDetails) {
        return userService.getProfile(userDetails);
    }

    @Override
    public UserResponse getUser(String username) {
        return userService.getUserByUsername(username);
    }

    @Override
    public List<UserResponse> getUserFriends(String username) {
        return userService.getUserFriends(username);
    }

    @Override
    public void deleteUser(UserDetails userDetails) {
        userService.deleteUser(userDetails);
    }

    @Override
    public UserResponse updateUser(UserDetails userDetails,
                                   @Valid UpdateUserRequest userRequest) {
        return userService.updateUser(userDetails, userRequest);
    }

    @Override
    public List<UserResponse> getUsers() {
        return userService.getActiveUsers();
    }

    @Override
    public void confirmUser(UUID userId) {
        userService.confirmUser(userId);
    }


}
