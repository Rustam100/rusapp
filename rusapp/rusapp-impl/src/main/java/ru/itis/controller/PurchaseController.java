package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.PurchaseApi;
import ru.itis.dto.request.CreatePurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.PurchaseResponse;
import ru.itis.service.PurchaseService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class PurchaseController implements PurchaseApi<UserDetails> {

    private final PurchaseService purchaseService;

    @Override
    public PurchaseResponse getPurchase(UUID purchaseId) {
        return purchaseService.getPurchase(purchaseId);
    }

    @Override
    public List<PurchaseResponse> getUserPurchases(String username, int page, int size) {
        return purchaseService.getUserPurchases(username, page, size);
    }

    @Override
    public List<PurchaseResponse> searchPurchasesByCriteria(int page, int size, String criteria) {
        return purchaseService.searchPurchasesByCriteria(page, size, criteria);
    }

    @Override
    public PurchaseResponse addNewPurchase(UserDetails userDetails, @Valid CreatePurchaseRequest createPurchaseRequest) {
        return purchaseService.addNewPurchase(userDetails, createPurchaseRequest);
    }

    @Override
    public PurchaseResponse updatePurchase(UserDetails userDetails, UUID purchaseId, @Valid UpdatePurchaseRequest request) {
        return purchaseService.updatePurchase(userDetails, purchaseId, request);
    }

    @Override
    public void deletePurchase(UserDetails userDetails, UUID purchaseId) {

        purchaseService.deletePurchase(userDetails, purchaseId);
    }
}
