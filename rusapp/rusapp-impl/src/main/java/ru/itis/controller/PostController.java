package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.PostApi;
import ru.itis.dto.request.CreatePostRequest;
import ru.itis.dto.request.UpdatePostRequest;
import ru.itis.dto.response.PostResponse;
import ru.itis.service.PostService;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class PostController implements PostApi<UserDetails> {

    private final PostService postService;

    @Override
    public PostResponse createPost(@Valid CreatePostRequest postRequest, UserDetails userPrincipal) {
        return postService.createPost(postRequest, userPrincipal);
    }

    @Override
    public List<PostResponse> getPosts(UserDetails userPrincipal, int page, int size) {
        return postService.getPosts(userPrincipal, page, size);
    }

    @Override
    public PostResponse getPost(String postId) {
        return postService.getPostById(postId);
    }

    @Override
    public PostResponse updatePostById(UserDetails userPrincipal, UpdatePostRequest postRequest, String postId) {
        return postService.updatePostById(userPrincipal, postRequest, postId);
    }

    @Override
    public void deletePostById(UserDetails userPrincipal, String postId) {
        postService.deletePostById(userPrincipal, postId);
    }

    @Override
    public List<PostResponse> getUserPosts(String username, int page, int size) {
        return postService.getUserPostsByUsername(username, page, size);
    }
}
