package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.PurchaseStatisticApi;
import ru.itis.dto.response.PurchaseStatisticResponse;
import ru.itis.service.StatisticsService;


@RestController
@RequiredArgsConstructor
public class PurchaseStatisticController implements PurchaseStatisticApi {

    private final StatisticsService statisticsService;

    @Override
    public PurchaseStatisticResponse getPurchaseStatistics(String username, String from, String to) {
        return statisticsService.getPurchaseStatistics(username, from, to);
    }

    @Override
    public PurchaseStatisticResponse getGeneralPurchaseStatistics(String username, String from, String to) {
        return statisticsService.geGeneralPurchaseStatistics(username, from, to);
    }
}
