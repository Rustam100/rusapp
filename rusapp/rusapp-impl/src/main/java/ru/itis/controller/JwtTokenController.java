package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.jwt.service.JwtTokenService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/token")
public class JwtTokenController {

    private final JwtTokenService jwtTokenService;

    @PostMapping(value = "/refresh")
    public TokenCoupleResponse updateTokens(@RequestBody TokenCoupleResponse tokenCoupleResponse) {
        return jwtTokenService.refreshAccessToken(tokenCoupleResponse);
    }

}
