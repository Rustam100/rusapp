package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.GeneralPurchaseApi;
import ru.itis.dto.request.CreateGeneralPurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.GeneralPurchaseResponse;
import ru.itis.enums.State;
import ru.itis.service.GeneralPurchaseService;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class GeneralPurchaseController implements GeneralPurchaseApi<UserDetails> {

    private final GeneralPurchaseService generalPurchaseService;

    @Override
    public GeneralPurchaseResponse getGeneralPurchase(UUID purchaseId) {
        return generalPurchaseService.getGeneralPurchase(purchaseId);
    }

    @Override
    public List<GeneralPurchaseResponse> getUserGeneralPurchases(int page, int size, String username) {
        return generalPurchaseService.getUserGeneralPurchases(page, size, username);
    }

    @Override
    public void confirmGeneralPurchase(UserDetails userDetails, UUID generalPurchaseId, State state) {
        generalPurchaseService.confirmGeneralPurchase(userDetails, generalPurchaseId, state);
    }

    @Override
    public List<GeneralPurchaseResponse> searchPurchasesByCriteria(int page, int size, String username, String criteria) {
        return generalPurchaseService.searchGeneralPurchasesByCriteria(page, size, username, criteria);
    }

    @Override
    public GeneralPurchaseResponse updatePurchase(UserDetails userDetails, UUID generalPurchaseId, UpdatePurchaseRequest request) {
        return generalPurchaseService.updatePurchase(userDetails, generalPurchaseId, request);
    }

    @Override
    public GeneralPurchaseResponse addGeneralPurchase(UserDetails userDetails, CreateGeneralPurchaseRequest request) {
        return generalPurchaseService.addGeneralPurchase(userDetails, request);
    }
}
