package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import ru.itis.enums.PostState;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "post")
@Data
public class PostEntity extends AbstractEntity {

    private String title;

    private String description;

    @Enumerated(EnumType.STRING)
    private PostState state;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private UserEntity author;

    @OneToMany(mappedBy = "post")
    @Where(clause = "state='OK'")
    private List<FileEntity> files;

}
