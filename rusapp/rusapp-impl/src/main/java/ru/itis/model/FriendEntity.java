package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;

@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name = "friends",
        uniqueConstraints = @UniqueConstraint(columnNames = {"requester_id", "friend_id"}))
public class FriendEntity extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "requester_id")
    private UserEntity requester;

    @ManyToOne
    @JoinColumn(name = "friend_id")
    private UserEntity friend;

    @Column(name = "is_active")
    private boolean isActive;

}