package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.Role;
import ru.itis.enums.State;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name = "account")
public class UserEntity extends AbstractEntity {

    @Column(unique = true, nullable = false, updatable = false)
    private String username;

    @Column(name = "first_name")
    private String firstname;

    @Column(name = "last_name")
    private String lastname;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(name = "hash_password", nullable = false)
    private String hashPassword;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
    private List<PostEntity> posts;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<PurchaseEntity> purchases;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY)
    private List<GeneralPurchaseEntity> generalPurchases;

}
