package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@Table(name = "purchase")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PurchaseEntity extends AbstractEntity {

    private String title;

    private String description;

    private Double cost;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private UserEntity customer;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    @OneToMany(mappedBy = "purchase")
    @Where(clause = "state='OK'")
    private List<FileEntity> files;

    @OneToMany(mappedBy = "purchase", fetch = FetchType.LAZY)
    private List<GeneralPurchaseEntity> generalPurchases;

}
