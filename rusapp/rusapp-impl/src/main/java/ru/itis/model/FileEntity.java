package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.FileState;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "file")
public class FileEntity extends AbstractEntity{

    @Column(name = "storage_file_name", nullable = false, unique = true)
    private String storageFileName;

    @Column(name = "original_file_name", nullable = false)
    private String originalFileName;

    @Column(nullable = false)
    private Long size;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(columnDefinition = "varchar(32) default 'OK'")
    @Enumerated(EnumType.STRING)
    private FileState state;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private PostEntity post;

    @ManyToOne
    @JoinColumn(name = "purchase_id")
    private PurchaseEntity purchase;


}
