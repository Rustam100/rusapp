package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account_refresh_token")
public class UserRefreshTokenEntity extends RefreshTokenEntity{

    @OneToOne
    @JoinColumn(name = "account_id")
    private UserEntity account;
}
