package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.State;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "general_purchase",
        uniqueConstraints = @UniqueConstraint(columnNames = {"purchase_id","account_id"}))
public class GeneralPurchaseEntity extends AbstractEntity{

    @ManyToOne
    @JoinColumn(name = "account_id")
    private UserEntity customer;

    @ManyToOne
    @JoinColumn(name = "purchase_id")
    private PurchaseEntity purchase;

    @Enumerated(EnumType.STRING)
    private State state;

}

