package ru.itis.constants;

public class Constant {

    public static final String USERNAME_REGEX = "^[a-zA-Z0-9._-]$";
    public static final String PASSWORD_REGEX  = "^[a-zA-Z0-9]$";
    public static final String EMAIL_REGEX = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
    public static final String NONE = "none";
}
