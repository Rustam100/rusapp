package ru.itis.constants;

public class PurchaseConstant {

    public static final String IN= ":";
    public static final String LIKE= "?";
    public static final String GREATER_THEN= "<";
    public static final String LESS_THEN= ">";
    public static final String EQUAL= "!";
}
