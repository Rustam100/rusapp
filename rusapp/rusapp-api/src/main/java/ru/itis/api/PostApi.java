package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreatePostRequest;
import ru.itis.dto.request.UpdatePostRequest;
import ru.itis.dto.response.PostResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;


@RequestMapping("/api/rusapp/posts")
public interface PostApi<PRINCIPAL> {

    @Operation(summary = "Получение постов пользователя с пагинацией(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список постов с пагинацией",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = PostResponse.class))
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<PostResponse> getPosts(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                @Parameter(description = "На какую страницу перейти", required = true)
                                @RequestParam("page") int page,
                                @Parameter(description = "Количество постов на странице",required = true)
                                @RequestParam("size") int size);




    @Operation(summary = "Получение поста по ID(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Данные поста",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PostResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/{post-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PostResponse getPost(@Parameter(description = "ID поста", required = true)
                         @PathVariable("post-id") String postId);




    @Operation(summary = "Получение постов пользователя с пагинацией по username(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список постов с пагинацией",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = PostResponse.class))
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/user/{username}")
    @ResponseStatus(HttpStatus.OK)
    List<PostResponse> getUserPosts(@Parameter(description = "Имя пользователя", required = true)
                                    @PathVariable String username,
                                    @Parameter(description = "На какую страницу перейти", required = true)
                                    @RequestParam("page") int page,
                                    @Parameter(description = "Количество постов на странице",required = true)
                                    @RequestParam("size") int size);




    @Operation(summary = "Добавление поста(потребуется авторизация")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Данные созданного поста",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PostResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    PostResponse createPost(@Parameter(description = "Форма создания поста")
                            @RequestBody CreatePostRequest postRequest,
                            @ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal);





    @Operation(summary = "Изменение поста по ID(потребуется авторизация")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Данные обновленного поста",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PostResponse.class)
                            )
                    }
            ),
            @ApiResponse(responseCode = "201", description = "Данные обновленного поста",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PostResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PutMapping(value = "/{post-id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PostResponse updatePostById(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                @Parameter(description = "Форма обновления поста")
                                @RequestBody UpdatePostRequest postRequest,
                                @Parameter(description = "ID поста")
                                @PathVariable("post-id") String postId);





    @Operation(summary = "Удаление поста по ID(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пост удален"
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping(value = "/{post-id}")
    @ResponseStatus(HttpStatus.OK)
    void deletePostById(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                        @Parameter(description = "ID поста")
                        @PathVariable("post-id") String postId);

}
