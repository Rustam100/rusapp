package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreatePurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.FileResponse;
import ru.itis.dto.response.PurchaseResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static ru.itis.constants.Constant.NONE;

@RequestMapping("/api/rusapp/purchase")
public interface PurchaseApi<PRINCIPAL> {


    @Operation(summary = "Получение покупки по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/{purchase-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PurchaseResponse getPurchase(@Parameter(description = "id покупки") @PathVariable("purchase-id")UUID purchaseId);



    @Operation(summary = "Получение всех покупок пользователя по его имени")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список покупок",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = PurchaseResponse.class)))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/user/{user}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<PurchaseResponse> getUserPurchases(@Parameter(description = "Имя пользователя")  @PathVariable("user") String username,
                                            @Parameter(description = "Номер страницы")    @RequestParam("page") int page,
                                            @Parameter(description = "Размер страницы")   @RequestParam("size") int size);


    @Operation(summary = "Поиск покупок по критериям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список покупок",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = PurchaseResponse.class)))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/user/search", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<PurchaseResponse> searchPurchasesByCriteria(@Parameter(description = "Номер страницы")   @RequestParam("page") int page,
                                                     @Parameter(description = "Размер страницы")  @RequestParam("size") int size,
                                                     @Parameter(description = "Критерии")         @RequestParam(name = "criteria", defaultValue = NONE) String criteria);


    @Operation(summary = "Добавление новой покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(value = "/addNew",consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    PurchaseResponse addNewPurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                    @Parameter(description = "Покупка") @RequestBody CreatePurchaseRequest createPurchaseRequest);


    @Operation(summary = "Изменение покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Измененная покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PutMapping(value = "/{purchase-id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PurchaseResponse updatePurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                    @Parameter(description = "id покупки") @PathVariable("purchase-id")UUID purchaseId,
                                    @RequestBody UpdatePurchaseRequest request);


    @Operation(summary = "Удаление покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Покупка успешно удалена")})
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping(value = "/{purchase-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    void deletePurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                        @Parameter(description = "id покупки") @PathVariable("purchase-id")UUID purchaseId);
}
