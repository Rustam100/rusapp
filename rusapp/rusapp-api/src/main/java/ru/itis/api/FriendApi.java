package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.response.UserResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


@RequestMapping("/api/rusapp/friends")
public interface FriendApi<PRINCIPAL> {

    @Operation(summary = "Получение друзей пользователя(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список друзей",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = UserResponse.class))
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    List<UserResponse> getFriends(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                  @Parameter(name = "Разделы", description = "Разделение пользователей по секциям. " +
                                          "Например, список заявок в друзья, список друзей, список исходящих заявок")
                                  @RequestParam(value = "section", required = false) String section);

    @Operation(summary = "Отправление запроса в друзья(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Заявка отправлена"
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(value = "/{username}")
    @ResponseStatus(HttpStatus.OK)
    void sendRequest(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                     @Parameter(name = "Имя пользователя", description = "Имя пользователя, которого мы хотим добавить в друзья")
                     @PathVariable("username") String friendUsername);

    @Operation(summary = "Принятие запроса в друзья(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос принят"
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PutMapping(value = "/{username}")
    @ResponseStatus(HttpStatus.OK)
    void acceptRequest(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                       @Parameter(name = "Имя пользователя", description = "Имя пользователя, заявку которого мы хотим принять")
                       @PathVariable("username") String requesterUsername,
                       @Parameter(description = "?action=accept -> принять, ?action=cancel -> отменить",required = true)
                       @RequestParam("action") String action);


    @Operation(summary = "Удаление из друзей(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь удален из друзей"
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping(value = "/{username}")
    @ResponseStatus(HttpStatus.OK)
    void deleteFriend(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                      @Parameter(description = "Имя пользователя, которого мы хотим удалить из друзей", required = true)
                      @PathVariable("username") String username);


}
