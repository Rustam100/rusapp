package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.FileResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RequestMapping("/api/rusapp/file")
public interface FileApi<PRINCIPAL> {


    @Operation(summary = "Получение файла по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Параметры файла",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = FileResponse.class))})
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/{file-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    FileResponse getFile(@Parameter(description = "id файла") @PathVariable("file-id") UUID fileId);



    @Operation(summary = "Добавление файлов и их привязка к посту")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Список файлов",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                      @Schema(implementation = FileResponse.class)))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(value = "/post/{post-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    List<FileResponse> uploadToPost(@ApiIgnore @AuthenticationPrincipal PRINCIPAL principal,
                                    @Parameter(description = "id поста")  @PathVariable("post-id") UUID postId,
                                    @Parameter(description = "файлы")     @RequestParam("files")MultipartFile[] multipartFiles);



    @Operation(summary = "Добавление файлов и их привязка к покупке")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Список файлов",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = FileResponse.class)))})
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(value = "/purchase/{purchase-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    List<FileResponse> uploadToPurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL principal,
                                        @Parameter(description = "id покупки")  @PathVariable("purchase-id") UUID purchaseId,
                                        @Parameter(description = "файлы")       @RequestParam("files")MultipartFile[] multipartFiles);


    @Operation(summary = "Удаление файла, привязанного к посту")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Файл успешно удален")
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping(value = "/post/{post-id}/{file-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deletePostFile(@ApiIgnore @AuthenticationPrincipal PRINCIPAL principal,
                        @Parameter(description = "id поста")  @PathVariable("post-id") UUID postId,
                        @Parameter(description = "id файла")  @PathVariable("file-id") UUID fileId);


    @Operation(summary = "Удаление файла, привязанного к покупке")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Файл успешно удален")
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping(value = "/purchase/{purchase-id}/{file-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deletePurchaseFile(@ApiIgnore @AuthenticationPrincipal PRINCIPAL principal,
                            @Parameter(description = "id покупки")  @PathVariable("purchase-id") UUID purchaseId,
                            @Parameter(description = "id файла")    @PathVariable("file-id") UUID fileId);
}
