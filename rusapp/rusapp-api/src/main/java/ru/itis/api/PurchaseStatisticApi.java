package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.response.PurchaseResponse;
import ru.itis.dto.response.PurchaseStatisticResponse;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static ru.itis.constants.Constant.NONE;

@RequestMapping("/api/rusapp/statistic")
public interface PurchaseStatisticApi {

    @Operation(summary = "Получение статистики пользователя по покупкам")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Статистика",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PurchaseStatisticResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "purchase/user/{username}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PurchaseStatisticResponse getPurchaseStatistics(@Parameter(description = "Имя пользователя") @PathVariable("username") String username,
                                                    @Parameter(description = "Начальная дата") @RequestParam(value = "from", defaultValue = NONE) String from,
                                                    @Parameter(description = "Конечная дата") @RequestParam(value = "to", defaultValue = NONE) String to);


    @Operation(summary = "Получение статистики пользователя по общим покупкам")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Статистика",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = PurchaseStatisticResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "general-purchase/user/{username}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    PurchaseStatisticResponse getGeneralPurchaseStatistics(@Parameter(description = "Имя пользователя") @PathVariable("username") String username,
                                                           @Parameter(description = "Начальная дата") @RequestParam(value = "from", defaultValue = NONE) String from,
                                                           @Parameter(description = "Конечная дата") @RequestParam(value = "to", defaultValue = NONE) String to);
}
