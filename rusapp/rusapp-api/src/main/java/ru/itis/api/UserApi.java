package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.UpdateUserRequest;
import ru.itis.dto.request.UserRequest;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.dto.response.UserResponse;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RequestMapping("/api/rusapp/users")
public interface UserApi<PRINCIPAL> {

    @Operation(summary = "Получение профиля пользователя(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Профиль пользователя",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/profile", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    UserResponse getProfile(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal);



    @Operation(summary = "Получение пользователя по username(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница пользователя",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{username}")
    @ResponseStatus(HttpStatus.OK)
    UserResponse getUser(@Parameter(description = "Имя пользователя", required = true)
                         @PathVariable("username") String username);




    @Operation(summary = "Получение друзей пользователя по username(потребуется авторизация")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список друзей пользователя",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = UserResponse.class))
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(produces = APPLICATION_JSON_VALUE, value = "/{username}/friends")
    @ResponseStatus(HttpStatus.OK)
    List<UserResponse> getUserFriends(@Parameter(description = "Имя пользователя", required = true)
                                      @PathVariable String username);




    @Operation(summary = "Получение списка пользователей(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список пользователей",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = UserResponse.class))
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<UserResponse> getUsers();


    @Operation(summary = "Подтверждение пользователя через email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь подтвержден"
            )
    })
    @GetMapping(value = "/confirm")
    @ResponseStatus(HttpStatus.OK)
    void confirmUser(@Parameter(description = "Код подтвеждения", required = true)
                     @RequestParam("code") UUID userId);




    @Operation(summary = "Регистрация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "ID пользователя",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UUID.class)
                            )
                    }
            )
    })
    @PostMapping(value = "/signup",consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    UUID createUser(@Parameter(description = "Форма регистрации пользователя")
                    @RequestBody CreateUserRequest userRequest);



    @Operation(summary = "Авторизация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Информация о токене",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = TokenCoupleResponse.class)
                            )
                    }
            )
    })
    @PostMapping(value = "/login", consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    TokenCoupleResponse login(@Parameter(description = "Форма для авторизации пользователя")
                              @RequestBody UserRequest userRequest);




    @Operation(summary = "Изменение данных пользователя(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленная информация о пользователе",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserResponse.class)
                            )
                    }
            ),
            @ApiResponse(responseCode = "201", description = "Обновленная информация о пользователе",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = UserResponse.class)
                            )
                    }
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    UserResponse updateUser(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                            @Parameter(description = "Форма для обновления данных пользователя")
                            @RequestBody UpdateUserRequest userRequest);




    @Operation(summary = "Удаление пользователя(потребуется авторизация)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь удален"
            )
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal);

}
