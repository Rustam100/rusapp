package ru.itis.api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreateGeneralPurchaseRequest;
import ru.itis.dto.request.UpdatePurchaseRequest;
import ru.itis.dto.response.GeneralPurchaseResponse;
import ru.itis.dto.response.PurchaseResponse;
import ru.itis.enums.State;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.UUID;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static ru.itis.constants.Constant.NONE;

@RequestMapping("/api/rusapp/general-purchase")
public interface GeneralPurchaseApi<PRINCIPAL> {


    @Operation(summary = "Получение общей покупки по id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Общая покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = GeneralPurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/{purchase-id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    GeneralPurchaseResponse getGeneralPurchase(@Parameter(description = "id общей покупки") @PathVariable("purchase-id")UUID purchaseId);


    @Operation(summary = "Получение всех общих покупок пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список общих покупок",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = GeneralPurchaseResponse.class)))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/user/{username}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<GeneralPurchaseResponse> getUserGeneralPurchases(@Parameter(description = "Номер страницы", required = true)    @RequestParam("page") int page,
                                                          @Parameter(description = "Размер страницы")     @RequestParam(value = "size") int size,
                                                          @Parameter(description = "Имя пользователя")    @PathVariable(name = "username") String username);


    @Operation(summary = "Подтверждение или удаление общей покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Покупка подтверждена или удалена успешно")
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/confirm/{general-purchase}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void confirmGeneralPurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                @Parameter(description = "id общей покупки") @PathVariable("general-purchase")UUID generalPurchaseId,
                                @Parameter(description = "Состояние")        @RequestParam("state")State state);


    @Operation(summary = "Поиск общих покупок по критериям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список покупок",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema =
                                    @Schema(implementation = GeneralPurchaseResponse.class)))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @GetMapping(value = "/user/search", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    List<GeneralPurchaseResponse> searchPurchasesByCriteria(@Parameter(description = "Номер страницы", required = true)    @RequestParam("page") int page,
                                                            @Parameter(description = "Размер страницы")     @RequestParam(value = "size") int size,
                                                            @Parameter(description = "Имя пользователя")    @RequestParam(name = "username", defaultValue = NONE) String username,
                                                            @Parameter(description = "Критерии сортировки") @RequestParam(name = "criteria", defaultValue = NONE) String criteria);



    @Operation(summary = "Добавление новой покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = GeneralPurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    GeneralPurchaseResponse addGeneralPurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                               @Parameter(description = "Общая покупка") @RequestBody CreateGeneralPurchaseRequest request);



    @Operation(summary = "Изменение общей покупки")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Измененная покупка",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema =
                                    @Schema(implementation = GeneralPurchaseResponse.class))}),
    })
    @ApiImplicitParam(name = "Authorization", paramType = "header", required = true)
    @PutMapping(value = "/{general-purchase}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    GeneralPurchaseResponse updatePurchase(@ApiIgnore @AuthenticationPrincipal PRINCIPAL userPrincipal,
                                           @Parameter(description = "id общей покупки") @PathVariable("general-purchase")UUID generalPurchaseId,
                                           @Parameter(description = "Общая покупка")    @RequestBody UpdatePurchaseRequest request);
}
