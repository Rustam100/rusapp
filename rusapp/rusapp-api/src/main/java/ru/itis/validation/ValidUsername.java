package ru.itis.validation;

import ru.itis.validation.impl.UsernameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UsernameValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUsername {

    String message() default "Username must contain numbers/letters, length must be longer 3";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default  {};



}
