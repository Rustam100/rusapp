package ru.itis.enums;

import lombok.Getter;

@Getter
public enum State {
    CONFIRMED,
    NOT_CONFIRMED,
    DELETE,
    BAN
}
