package ru.itis.enums;

import lombok.Getter;

@Getter
public enum Role {
    ADMIN,
    USER
}
