package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.Role;
import ru.itis.enums.State;

import java.util.UUID;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    @Schema(description = "ID пользователя")
    private UUID uuid;

    @Schema(description = "Имя", example = "Рустем")
    private String firstname;

    @Schema(description = "Фамилия", example = "Каримов")
    private String lastname;

    @Schema(description = "Имя пользователя", example = "hauntedo")
    private String username;

    @Schema(description = "Электронная почта пользователя", example = "rustem.karimov.2002@gmail.com")
    private String email;

    @Schema(description = "Роль пользователя", allowableValues = {"ADMIN", "USER"})
    private Role role;

    @Schema(description = "Статус пользователя", allowableValues = {"DELETE", "BAN", "NOT_CONFIRMED", "CONFIRMED"})
    private State state;

}
