package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Профиль")
public class ProfileResponse extends UserResponse {

    @Schema(description = "Список покупок")
    private List<PurchaseResponse> purchases;

    @Schema(description = "Список постов")
    private List<PostResponse> posts;
}
