package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.validation.ValidPassword;
import ru.itis.validation.ValidUsername;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма авторизации пользователя(используется для регистрации)")
public class UserRequest {

    @Schema(description = "Имя пользователя", example = "hauntedo")
    @ValidUsername
    private String username;

    @Schema(description = "Пароль пользователя", example = "qwerty007")
    @ValidPassword
    private String password;

}