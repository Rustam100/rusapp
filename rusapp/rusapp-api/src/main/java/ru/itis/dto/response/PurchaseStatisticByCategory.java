package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PurchaseStatisticByCategory {

    @Schema(description = "Общая сумма потраченных денег")
    private Double sum;

    @Schema(description = "Количество совершенных покупок")
    private Long count;

    @Schema(description = "Название категории")
    private String category;

}
