package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Общая покупка")
public class GeneralPurchaseResponse extends PurchaseResponse{

    @Schema(description = "Общая цена")
    private Double generalCost;

    @Schema(description = "Список друзей")
    private List<String> friends;

}
