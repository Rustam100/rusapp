package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Параметры файла")
public class FileResponse {

    @Schema(description = "id файла")
    private UUID uuid;

    @Schema(description = "Название файла")
    private String originalFileName;

    @Schema(description = "Размер файла")
    private Long size;

    @Schema(description = "Тип передаваемого файла")
    private String contentType;
}
