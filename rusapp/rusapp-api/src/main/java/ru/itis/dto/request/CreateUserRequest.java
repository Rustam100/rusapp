package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import static ru.itis.constants.Constant.EMAIL_REGEX;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Форма регистрации пользователя")
public class CreateUserRequest extends UserRequest {

    @Schema(description = "Имя", example = "Рустем")
    @NotBlank(message = "First name mustn't be empty")
    private String firstname;

    @Schema(description = "Фамилия", example = "Каримов")
    @NotBlank(message = "Last name mustn't be empty")
    private String lastname;

    @Schema(description = "Электронная почта", example = "rustem.karimov.2002@gmail.com")
    @Email(regexp = EMAIL_REGEX, message = "Invalid email")
    private String email;

}
