package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Общая покупка")
public class CreateGeneralPurchaseRequest extends CreatePurchaseRequest{

    @Schema(description = "Список друзей", example = "[rustam5316, oleg5316]")
    private List<String> friends;
}
