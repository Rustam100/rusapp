package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Data
@Schema(description = "Форма обновления профиля пользователя")
public class UpdateUserRequest extends UserRequest {

    @Schema(description = "Имя", example = "Рустем")
    private String firstname;

    @Schema(description = "Фамилия", example = "Каримов")
    private String lastname;

}

