package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePurchaseRequest {

    @Size(min = 2, max = 100, message = "Title must be between 2 and 100 characters")
    @Schema(description = "Новое название", example = "Macbook")
    private String title;

    @Size(min = 10, max = 500, message = "Description must be between 10 and 500 characters")
    @Schema(description = "Новое описание", example = "i bought this device for study")
    private String description;

    @Positive(message = "Cost should be greater than 0")
    @Schema(description = "Новая цена", example = "99999.9")
    private Double cost;

    @Schema(description = "Новая категория", example = "Computer")
    private String CategoryTitle;
}
