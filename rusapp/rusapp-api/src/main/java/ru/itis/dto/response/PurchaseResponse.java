package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Покупка")
public class PurchaseResponse {

    @Schema(description = "id покупки")
    private UUID uuid;

    @Schema(description = "Название")
    private String title;

    @Schema(description = "Описание")
    private String description;

    @Schema(description = "Цена")
    private Double cost;

    @Schema(description = "Дата совершения покупки")
    private Instant createDate;

    @Schema(description = "Категория")
    private CategoryResponse category;

    @Schema(description = "Список файлов")
    private List<FileResponse> files = new ArrayList<>();

}
