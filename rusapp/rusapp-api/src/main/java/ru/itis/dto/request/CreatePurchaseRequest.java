package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Покупка")
public class CreatePurchaseRequest {

    @NotBlank(message = "Title mustn't be empty")
    @Schema(description = "Название покупки", example = "MacBook")
    private String title;

    @Size(min = 10, max = 500, message = "Description must be between 10 and 500 characters")
    @Schema(description = "Описание покупки", example = "I bought this device for study. While all is good.")
    private String description;

    @Positive(message = "Cost should be greater than 0")
    @Schema(description = "Описание покупки", example = "120000.9")
    private Double cost;

    @Schema(description = "Название категории", example = "Device")
    private String categoryTitle;

}
