package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Критерии для поиска покупок")
public class SearchPurchasesCriteria {

    @Schema(description = "Операция", example = "?")
    private String operation;

    @Schema(description = "Поле", example = "customer")
    private String key;

    @Schema(description = "Значение", example = "Rustam")
    private Object value;
}
