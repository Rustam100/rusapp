package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Пост", description = "Добавление поста")
public class PostRequest {

    @Schema(description = "Название поста", example = "Кроссовки")
    @NotBlank(message = "Title mustn't be empty")
    private String title;

}
