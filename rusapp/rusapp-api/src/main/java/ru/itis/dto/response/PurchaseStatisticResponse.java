package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseStatisticResponse {

    @Schema(description = "Итоговая сумма затраченных денег")
    private Double totalSum;

    @Schema(description = "Общее количество покупок")
    private Long totalCount;

    @Schema(description = "Список статистики по категориям")
    List<PurchaseStatisticByCategory> purchaseStatisticByCategories;
}
