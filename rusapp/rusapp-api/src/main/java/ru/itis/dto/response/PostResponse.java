package ru.itis.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.PostState;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Пост")
public class PostResponse {

    @Schema(description = "ID поста")
    private UUID uuid;

    @Schema(description = "Название поста")
    private String title;

    @Schema(description = "Содержимое поста")
    private String description;

    @Schema(description = "Состояние поста", allowableValues = {"DELETED", "NOT_DELETED"})
    private PostState state;

    @Schema(description = "Список файлов, которые прикреплены к посту")
    private List<FileResponse> files = new ArrayList<>();

}
