package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Size;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Пост (добавление)")
public class CreatePostRequest extends PostRequest {

    @Schema(description = "Содержимое поста", example = "Этот пост написан с целью..")
    @Size(min = 10, max = 500, message = "Description must be between 10 and 500 characters")
    private String description;

}
