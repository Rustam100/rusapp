package ru.itis.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Schema(description = "Форма обновления поста")
public class UpdatePostRequest {

    @Schema(description = "Название поста", example = "Кроссовки")
    private String title;

    @Schema(description = "Содержимое поста", example = "Эти кроссовки подходят..")
    @Size(min = 10, max = 500, message = "Description must be between 10 and 500 characters")
    private String description;
}
